import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class Btree {
	private int M = 4; // max children per B-tree node = M-1

	private Node root; // root of the B-tree
	private int height; // height of the B-tree
	private int n; // number of elements in the B-tree

	// helper B-tree node data type
	private class Node {
		private int c; // number of children
		private Entry[] children = new Entry[M]; // the array of children

		private Node(int k) {
			this.c = k; // create a node with k children
		}
		
		public void getChildren() {
			Entry[] ar = this.children;
			// String[] words = new String[ar.length];
			for (int i = 0; i < ar.length - 1; i++)
				System.out.println(ar[i].word);
		}
		
	}

	// internal nodes: only use word and next
	// external nodes: only use word
	private static class Entry {
		private String word;
		private Node next =null;
		public Entry(String word, Node next) {
			this.word = word;
			this.next = next;
		}

		public String toString() {
			return word;

		}
	}


	// constructor
	public Btree() {
		this.root = new Node(0);
	}

	// return number of pairs in the B-tree
	public int size() {
		return this.n;
	}

	// return height of B-tree
	public int height() {
		return this.height;
	}

	public Node getRoot() {
		return this.root;
	}

	// search for given key, return associated value; return null if no such key
	public Entry[] get(String s) {
		return search(this.root, s, this.height);
	}

	// performs search only for the whole word
	private Entry[] search(Node x, String word, int ht) {
		word.toLowerCase();
		Entry[] children = x.children;
		ArrayList<Entry> ch = new ArrayList();
		for (Entry c : children)
			ch.add(c);
		// external node
		if (ht == 0) {
			for (int i = 0; i < x.c; i++) {
				// if (children[i].word.startsWith(word)){
				// ArrayList<Entry> ar = new ArrayList();
				// ar.add(children[i]);
				// for (Entry e: children[i].next.children)
				// ar.add(e);
				// //ar.add(children[i].next.children);
				// return ar;
				if (equal(word, children[i].word)) {
				//if (children[i].word.startsWith(word)){
					// return children[i].word;
					// Entry[] k = null;
					// k = combine(children, children[i].next.children);
					// if (children[i].next!=null)
					// if (children[i].next.children.length>0){
					// k = combine(children,children[i].next.children);
					// }
					// k = searchRecursively(children[i].next.children);
					// k = combine(children, k);
						//x.getChildren();
						return children;
				}
			}
		}

		// internal node
		else {
			for (int i = 0; i < x.c; i++) {
				 if (i + 1 == x.c || earlier(word, children[i + 1].word))
				return search(children[i].next, word, ht - 1);
			}
		}
		return null;
	}
	
	public boolean later(String a, String b){
			return b.compareToIgnoreCase(a) > 0;
	}

	public Entry[] combine(Entry[] a, Entry[] b) {
		int length = a.length + b.length;
		Entry[] result = new Entry[length];
		System.arraycopy(a, 0, result, 0, a.length);
		System.arraycopy(b, 0, result, a.length, b.length);
		return result;
	}

	// public Entry[] searchRecursively(Entry[] e){
	// for (Entry k :e)
	// e.
	// return e;
	// }

	// insert key-value pair
	// add code to check for duplicate keys
	public void put(String word) {
		Node u = insert(this.root, word, this.height);
		this.n++;
		if (u == null)
			return;

		// need to split root
		Node t = new Node(2);
		t.children[0] = new Entry(this.root.children[0].word, this.root);
		t.children[1] = new Entry(u.children[0].word, u);
		this.root = t;
		this.height++;
	}

	private Node insert(Node h, String word, int ht) {
		int j;
		Entry t = new Entry(word, null);
		// check for duplicates in the tree: make sure that no such word exists
		// in the tree
		if (get(word) != null)
			return null;

		// external node
		if (ht == 0) {
			for (j = 0; j < h.c; j++) {
				if (earlier(word, h.children[j].word))
					break;
			}
		}

		// internal node
		else {
			for (j = 0; j < h.c; j++) {
				if ((j + 1 == h.c) || earlier(word, h.children[j + 1].word)) {
					// if (get(word)!=null)
					// break;
					Node u = insert(h.children[j++].next, word, ht - 1);
					if (u == null)
						return null;
					t.word = u.children[0].word;
					t.next = u;
					break;
				}
			}
		}

		for (int i = h.c; i > j; i--)
			h.children[i] = h.children[i - 1];
		h.children[j] = t;
		h.c++;
		if (h.c < this.M)
			return null;
		else
			return split(h);
	}

	// split node in half
	private Node split(Node h) {
		Node t = new Node(M / 2);
		h.c = M / 2;
		for (int j = 0; j < M / 2; j++)
			t.children[j] = h.children[M / 2 + j];
		return t;
	}

	// for debugging
	public String toString() {
		return toString(this.root, this.height, "") + "\n";
	}

	private String toString(Node h, int ht, String indent) {
		String s = "";
		Entry[] children = h.children;

		if (ht == 0) {
			for (int j = 0; j < h.c; j++) {
				s += indent + children[j].word + " " + "\n";
			}
		} else {
			for (int j = 0; j < h.c; j++) {
				if (j > 0)
					s += indent + "(" + children[j].word + ")\n";
				s += toString(children[j].next, ht - 1, indent + "     ");
			}
		}
		return s;
	}

	// check if one String is lexicographically less than another
	private boolean earlier(String s1, String s2) {
		return s2.compareToIgnoreCase(s1) < 0;
	}

	private boolean equal(String s1, String s2) {
		return s1.compareToIgnoreCase(s2) == 0;
	}

	@SuppressWarnings("resource")
	public static StringBuilder readFromFile() {
		BufferedReader br;
		StringBuilder sb = new StringBuilder();
		try {
			br = new BufferedReader(new FileReader("wordBank.txt"));

			String line = br.readLine();
			while (line != null) {
				sb.append(line);
				sb.append(" ");
				line = br.readLine();
			}
		}

		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sb;

	}

	public static void main(String[] args) {
		StringBuilder sbu = new StringBuilder();
		sbu = readFromFile();
		Btree st = new Btree();

		String[] lines = sbu.toString().split(" ");
		for (String s : lines) {
			st.put(s);
		}
		System.out.println(st.toString());
		// st.getChildren();
		// System.out.println(st.size());
		Entry[] r=null;
		if ((r = st.get("love"))!=null)
			for (Entry l: r)
				System.out.println(l);
		// st.getChildren();
	}
}
